import TelegramBot from 'node-telegram-bot-api';
import mongoose from 'mongoose';

mongoose.Promise = global.Promise;

const ADMIN_CHAT = process.env.ADMIN_CHAT;

const USER_CLEAR = 0;
const USER_WARNED = 1;
const USER_KICKED = 2;
const USER_DEAD = 3;

const token = process.env.TELEGRAM_BOT_TOKEN;
let bot = null;
let connectionString = '';
if (process.env.NODE_ENV == 'development') {
  bot = new TelegramBot(token, {polling: true});
  connectionString = 'mongodb://localhost/chatbot';
} else {
  // See https://developers.openshift.com/en/node-js-environment-variables.html
  const port = process.env.OPENSHIFT_NODEJS_PORT;
  const host = process.env.OPENSHIFT_NODEJS_IP;
  const domain = process.env.OPENSHIFT_APP_DNS;
  bot = new TelegramBot(token, {webHook: {port, host}});
  bot.setWebHook(domain + ':443/bot' + token);
  connectionString = process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME;
}
mongoose.connect(connectionString);

const userSchema = mongoose.Schema({
  id: Number,
  username: String,
  first_name: String,
  last_name: String,
  warnings: Number,
  rating: Number,
});

const User = mongoose.model('User', userSchema);

bot.on('message', (async function (msg) {
  const chatId = msg.chat.id;
  const userId = msg.from.id;
  let user = await User.findOne({id: userId});
  if (!user) {
    let userData = msg.from;
    userData.rating = 0;
    userData.warnings = USER_CLEAR;
    user = new User(userData);
    await user.save();
  }
  if (
    msg.reply_to_message &&
    msg.text &&
    (msg.text.trim() == '+' || msg.text.trim() == '-') &&
    msg.chat &&
    msg.chat.type == 'supergroup'
  ) {
    let originMessage = msg.reply_to_message;
    let author = originMessage.from;
    let authorModel = await User.findOne({id: author.id});
    const increment = (msg.text.trim() == '+')?1:-1;
    if (!authorModel) {
      let userData = msg.from;
      userData.rating = 0;
      userData.warnings = USER_CLEAR;
      authorModel = new User(userData);
    }
    if (authorModel.rating == 0) {
      authorModel.rating = increment;
    } else {
      authorModel.rating = authorModel.rating + Math.floor(increment / Math.min(Math.abs(authorModel.rating), 10) * 10) / 10;
    }
    await authorModel.save();
  }
  if ((msg.text.trim() == '/karma') && msg.chat && (msg.chat.type == 'private') ) {
    bot.sendMessage(chatId, `Рейтинг: ${user.rating}`, {reply_to_message_id: msg.message_id});
  }
  if (msg.sticker) {
    let adminMessage = false;
    let nextStatus = false;
    switch (user.warnings) {
      case USER_CLEAR:
        bot.sendMessage(chatId, "Стикеры в этом чате запрещены! За вторую попытку вас выкинет из чата, а за третью - бан навсегда!", {reply_to_message_id: msg.message_id});
        adminMessage = `#стикер 1 предупреждение (${JSON.stringify(msg.from)})`;
        nextStatus = USER_WARNED;
        break;
      case USER_WARNED:
        await bot.sendMessage(chatId, "Стикеры в этом чате запрещены! Вы были предупреждены.", {reply_to_message_id: msg.message_id});
        adminMessage = `#стикер 2 предупреждение (${JSON.stringify(msg.from)})`;
        nextStatus = USER_KICKED;
        setTimeout(() => {
          bot.kickChatMember(chatId, userId)
            .then(() => bot.unbanChatMember(chatId, userId));
        }, 1000);
        break;
      case USER_KICKED:
        await bot.sendMessage(chatId, "Стикеры в этом чате запрещены! Вы были предупреждены. Постоянный бан.", {reply_to_message_id: msg.message_id});
        adminMessage = `#стикер 1 предупреждение (${JSON.stringify(msg.from)})`;
        nextStatus = USER_DEAD;
        setTimeout(() => {
          bot.kickChatMember(chatId, userId);
        }, 1000);
        break;
    }
    if (nextStatus) {
      user.warning = nextStatus;
      user.save();
    }
    if (adminMessage) {
      bot.sendMessage(ADMIN_CHAT, adminMessage);
      bot.forwardMessage(ADMIN_CHAT, chatId, msg.message_id);
    }
  }
}));
